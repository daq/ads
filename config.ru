require "sinatra.rb"
require "init.rb"

Main.set :run, false
Main.set :environment, :development

run Sinatra.application