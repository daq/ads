Sequel.migration do
    up do
        create_table :users do
            primary_key	:id
            Integer     :uid
            String      :nickname
           	String		:name
            String      :provider
            String      :image
            Timestamp   :created_at
        end

        create_table :ads do
            primary_key :id
            Text		:description
            Text		:description_html
            foreign_key	:user_id
            String      :city
            String      :category
            Timestamp	:created_at
            Timestamp	:updated_at
        end

        create_table :tags do
            primary_key :id
            String      :tag_name
            Timestamp   :created_at
            Timestamp   :updated_at
        end

        create_table :ads_tags do
            primary_key :id
            foreign_key :ad_id
            foreign_key :tag_id
        end
    end

    down do
        drop_table      :users
        drop_table      :ads
        drop_table      :tags
        drop_table      :tags_ads
    end

end