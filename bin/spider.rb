# coding: utf-8

require "yaml"
require "open-uri"
require "nokogiri"
require "sequel"

class Spider

  attr_reader :config

  def initialize
    #@config = YAML::load(File.open(fname))
    @DB = Sequel.connect("mysql2://root:root@localhost:3306/ads_dev", :encoding => "UTF8")
  end

  def import_models
    Dir[File.join(File.dirname(__FILE__), "app", "models", "*.rb")].each do |file|
      require file
    end
  end

  def parse(doc)
    "its a stub"
  end

  def get_city(text)
    "its  a stub"
  end

  def walk
    "its a stub"  
  end

  def save(data)
    ad = Ad.create(:description => data["description"],
      :description_html => data["description"],
      :user_id => "1",
      :created_at => Time.now,
      :updated_at => Time.now)
    data['tags'].each { |tag| 
      t = Tag.find_or_create(:tag_name => tag,
        :created_at => Time.now,
        :updated_at => Time.now)
      t.add_ad(ad)
    }
  end

end

class Chastnik < Spider

  def parse(doc)
    doc.xpath('//ul[@class="reset pathway"]//a').each { |cat|
      puts cat.content
    }
    doc.xpath('//div[@class="sidebar sb-right"]//p').each {|phone|
      puts phone.content
    }
    doc.xpath('//div[@class="content-wsbr"]//p').each {|phone|
      puts phone.content
    }

  end

  def get_city(text)
    "its  a stub"
  end

  def walk
    i = 1
    tm = Time.now
    ntm = tm + 604800
    now_day = tm.day
    now_month = tm.month
    now_year = tm.year
    next_day = ntm.day
    next_month = ntm.month
    next_year = ntm.year
    while true
      source = open("http://www.chastnik.ru/category/adv_ob/page/" + i.to_s + "/?st&ct=500&dperiod=1&dfrom_d=09&dfrom_m=05&dfrom_y=2012&dto_d=11&dto_m=05&dto_y=2012&dorder=1&npaged=50").read
      source.scan(/http:\/\/www\.chastnik\.ru\/\d\d\d\d\/\d\d\/\d\d\/\d+\//).each {|link|
        puts link
        doc = Nokogiri::HTML(open(link))
        parse(doc)
      }
      i += 1
    end  
  end

end

class Mspros < Spider

  def parse(doc)
    doc.xpath('//index//p[@class="obvl_text_rub"]').each { |cat|
      puts cat.content
    }
    doc.xpath('//index//p[@class="obvl_text"]').each { |cat|
      puts cat.content
    }
  end

  def get_city(text)
    "its  a stub"
  end

  def walk
    i = 0
    while true
      source = "http://mspros.ru/obvl/?search=1&search_str=&search_rub=17&search_podrub=11&search_page=" + i.to_s
      doc = Nokogiri::HTML(open(source))
      parse(doc)
      i += 1
    end  
  end

end

c = Mspros.new
c.walk