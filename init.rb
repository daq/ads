require "sequel"
require "sinatra"
require "slim"
require "unicode_utils"
require "omniauth"
require "omniauth-twitter"
require "yaml"
require "logger"
require "json"

DB = Sequel.connect("mysql2://root:root@localhost:3306/ads_dev", :encoding => "UTF8", :loggers => [Logger.new($stdout)])

use Rack::Session::Cookie # OmniAuth requires sessions
use OmniAuth::Builder do
  provider :twitter, "kVT21SRy272jjWaJom5bA", "88HSmorc9879rlECnQBQYzQ697ef4xjR7seoJt9CM"
end

Slim::Engine.set_default_options :pretty => true
OmniAuth.config.test_mode = true

if defined? Encoding
  Encoding.default_external = Encoding::UTF_8
  Encoding.default_internal = Encoding::UTF_8
end

YAML::ENGINE.yamler = 'syck'
conf = YAML.load(File.read("#{File.dirname(__FILE__)}/config/config.yml"))
p conf  
set :root, File.dirname(__FILE__)
set :app_file, __FILE__
set :default_encoding, 'utf-8'
set :sessions, true
set :static, true

set :menu, conf['categories']
set :title, conf['title']
set :cities, conf['cities']
set :default_city, conf['cities'][conf['default_city']]
set :keywords, conf['keywords']

# Load all application files
Dir[File.join(File.dirname(__FILE__), "app", "**", "*.rb")].each do |file|
  require file
end
