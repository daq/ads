class Ad < Sequel::Model
	many_to_one    :user
	many_to_many   :tags
end