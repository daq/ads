# coding: utf-8

get '/' do
  @ads = Ad.filter(:city => settings.default_city).order(:created_at.desc).limit(30)
  slim :index
end

get '/new' do
  if current_user
    slim :new
  else
    redirect '/'
  end
end

get '/tag/:mytag' do
  tag = Tag[:tag_name => params['mytag']]
  @ads = tag.ads
  slim :index
end

get '/ajx' do
  @ads = Ad.filter(:city => @@conf['cities'][@@conf['default_city']]).order(:created_at.desc).limit(30)
  content_type 'text/html', :charset => 'utf-8'
  slim :ads, :layout => false
end

post '/' do
  if params_ok([params["description"], params['tags'], params['city'], params['cat'], params['subcat']]) and current_user
    ad = Ad.create(:description => params["description"],
      :description_html => to_html(params["description"]),
      :user_id => session[:user_id],
      :city => params['city'],
      :category => [params['cat'], params['subcat']].join('#'),
      :created_at => Time.now,
      :updated_at => Time.now)
    current_user.add_ad(ad)
    get_tags(params["tags"]).each { |tag|
      t = Tag[:tag_name => tag]
      if t.nil?
        t = Tag.create( :tag_name => tag,
          :created_at => Time.now,
          :updated_at => Time.now)
      end
      t.add_ad(ad)
    }
  end
  redirect '/'
end

get '/auth/:provider/callback' do
  @auth = request.env['omniauth.auth']
  usr = User.find_or_create(:uid => @auth["uid"], 
    :nickname => @auth["info"]["nickname"], 
    :name => @auth["info"]["name"],
    :provider => @auth["provider"],
    :image => @auth["info"]["image"],
    :created_at => Time.now)
  session[:user_id] = usr.id
  redirect request.env['omniauth.origin'] || '/'
end

get '/*' do
  404
end