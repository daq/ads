# -*- coding: utf-8 -*-

helpers do

  def current_user
    @current_user ||= User[session[:user_id]] if session[:user_id]
  end

  def current_user_name
    @current_user.name if current_user
  end

  def current_user_image
    @current_user.image if current_user
  end

  def get_tags (str)
    str.gsub(/\s+/, '').split(',').map{|item| UnicodeUtils.downcase(item)}
  end

  def to_html (ad)
    ad.sub('-', '').gsub(/(\+?\d{11})/, '<code>\1</code>')
  end

  def show_tags (ad)
    ad.tags_dataset.map { |tag|
      "<code><a href=/tag/" + tag[:tag_name] + ">" + tag[:tag_name] + "</a></code>"
    }.join
  end

  def params_ok(data)
    not (data.include?(nil) || data.include?(''))
  end

  def get_last_digit (num)
    tm = num.modulo(10)
    if tm == 0
      tm
    elsif tm > 9
      get_last_digit(tm)
    else
      tm
    end
  end

  def time_to_now (created_at)
    minutes = {0 => " минут",
      1 => " минуту",
      2 => " минуты",
      3 => " минуты",
      4 => " минуты",
      5 => " минут",
      6 => " минут",
      7 => " минут",
      8 => " минут",
      9 => " минут"}
    hours = {0 => " часов",
      1 => " час",
      2 => " часа",
      3 => " часа",
      4 => " часа",
      5 => " часов",
      6 => " часов",
      7 => " часов",
      8 => " часов",
      9 => " часов"}
    days = {0 => " дней",
      1 => " день",
      2 => " дня",
      3 => " дня",
      4 => " дня",
      5 => " дней",
      6 => " дней",
      7 => " дней",
      8 => " дней",
      9 => " дней"}
    months = {0 => " месяцев",
      1 => " месяц",
      2 => " месяца",
      3 => " месяца",
      4 => " месяца",
      5 => " месяцев",
      6 => " месяцев",
      7 => " месяцев",
      8 => " месяцев",
      9 => " месяцев"}
    years = {0 => " лет",
      1 => " год",
      2 => " года",
      3 => " года",
      4 => " года",
      5 => " лет",
      6 => " лет",
      7 => " лет",
      8 => " лет",
      9 => " лет"}
    td = Time.now - created_at
    res = ""
    if td < 60
      return "Менее минуты назад"
    elsif td < 3600
      j = (td/60).floor
      i = get_last_digit(j)
      if j > 20 or j < 10
        puts i, j
        res = j.to_s + minutes[i]
      else
        res = j.to_s + " минут"
      end
    elsif td < 86400
      j = (td/3600).floor
      i = get_last_digit(j)
      if j > 20 or j < 10
        res = j.to_s + hours[i]
      else
        res = j.to_s + " часов"
      end
    elsif td < 2592000
      j = (td/86400).floor
      i = get_last_digit(j)
      if (j < 110 and j > 20) or j < 10 or (j < 210 and j > 120) or (j < 310 and j > 220)
        res = j.to_s + days[i]
      else
        res = j.to_s + " дней"
      end
    else 
      j = (td/2592000).floor
      i = get_last_digit(j)
      if j > 20 or j < 10
        res = j.to_s +years[i]
      else
        res = j.to_s + " лет"
      end

    end
    res + " назад"
  end

end